$fa=10;
outer_diameter	= 100;
outer_radius	= outer_diameter/2;
inner_diameter	= 40;
inner_radius	= inner_diameter/2;
hole_diameter	= 14;
hole_radius		= hole_diameter/2;
wall_thickness	= .75;
height			= 10;
outer_rim_width	= height;
support_width	= outer_rim_width * (2/3);
num_of_supports	= 6;
difference (){
	cylinder ( r=outer_radius, h=height, center=true);
// the outer rim
	cylinder ( r=outer_radius+wall_thickness, h = height-wall_thickness, center=true );
//	removes the spool section
	cylinder ( r=inner_radius-wall_thickness,h=height+wall_thickness, center=true);
// this removes the center piece, leaving a wall behind (I think)
	cylinder ( r=outer_radius-outer_rim_width, h=height+wall_thickness, center=true);
}
difference(){
	cylinder ( r=inner_radius, h=height, center=true);
//	the center support... or wheel part I guess?
	translate([0,0,wall_thickness]){
		cylinder ( r=inner_radius-wall_thickness*2, h=height/2);
	}
// these two translate() blocks contain the cutouts. These are just here to save plastic, there's no reason for it to be solid through.
	translate([0,0,-height/2-wall_thickness]){
		cylinder ( r=inner_radius-wall_thickness, h=height/2);
	}
	cylinder ( r=hole_radius, h=height, center=true);
	//cuts out a hole for the center
}
// then add the lip
difference (){
	cylinder ( r=hole_radius+wall_thickness, h=height/3, center=true );
	cylinder ( r=hole_radius, h=height, center=true);
}
// this is a reusable module for an individual support beam, positioned
// between the inner and outer rims.
module support_beam(){
	translate ([ -support_width / 2, 
		inner_radius - wall_thickness * 2,
		height / 2 - wall_thickness / 2
	]){	cube ([ support_width,
		outer_radius - inner_radius - outer_rim_width + wall_thickness * 4,
		wall_thickness/2 ]);
	}
}
// then this module loops over the number of supports, placing the 
// individual supports at equal angles.
module top_supports() {
	for ( iter = [ 1 : num_of_supports ]) {
		rotate ([ 0,0, (360/num_of_supports)*iter ]) support_beam();
	}
}
top_supports(); 
// I think it looks better and might be slightly 
// stronger to offset the opposing supports
rotate ([ 0,0,360/(num_of_supports*2)]) 
	translate ([ 0,0,-(height-wall_thickness/2)])
	top_supports();